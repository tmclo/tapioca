package main

import (
	"github.com/gdamore/tcell/v2"
	"github.com/rivo/tview"
)

type User struct {
	email    string
	password string
}

type Domain struct {
	name string
}

type UserPW struct {
	email string
	newpw string
}

type DelUser struct {
	email string
}

type Whitelist struct {
	email string
}

type Blacklist struct {
	email string
}

var users []User
var domains []Domain
var userpws []UserPW
var delusers []DelUser
var whitelists []Whitelist
var blacklists []Blacklist

var app = tview.NewApplication()
var pages = tview.NewPages()
var form = tview.NewForm().SetFieldBackgroundColor(tcell.Color57).SetLabelColor(tcell.Color21)
var dmnform = tview.NewForm().SetFieldBackgroundColor(tcell.Color57).SetLabelColor(tcell.Color21)
var upwform = tview.NewForm().SetFieldBackgroundColor(tcell.Color57).SetLabelColor(tcell.Color21)
var rmusrform = tview.NewForm().SetFieldBackgroundColor(tcell.Color57).SetLabelColor(tcell.Color21)
var wlform = tview.NewForm().SetFieldBackgroundColor(tcell.Color57).SetLabelColor(tcell.Color21)
var blform = tview.NewForm().SetFieldBackgroundColor(tcell.Color57).SetLabelColor(tcell.Color21)
var list = tview.NewList().
	SetSecondaryTextColor(tcell.Color201).
	SetSelectedBackgroundColor(tcell.Color57).
	SetShortcutColor(tcell.Color21)

func cycleFocus(app *tview.Application, elements []tview.Primitive) {
	for i, el := range elements {
		if el.HasFocus() {
			continue
		}

		app.SetFocus(elements[i])
		return
	}
}

// add this to bl/wl
//
// tview.NewTextView().
// SetTextAlign(tview.AlignCenter).
// SetText("\n\nYou must specify an email or domain in the following format:\n\n\nEmail: email@example.com\n\nDomain: *@example.com\n\n")

func main() {
	newPrimitive := func(text string) tview.Primitive {
		return tview.NewTextView().
			SetTextAlign(tview.AlignCenter).
			SetText(text).
			SetTextColor(tcell.ColorHotPink)
	}

	// pages
	pages.AddPage("Welcome",
		tview.NewTextView().
			SetTextAlign(tview.AlignCenter).
			SetText("\n\nWelcome to Tapioca Mail Server Manager\n\n\nPlease select an action from the menu on the left to begin\n\n\nYou can switch between menus by pressing <tab>\n"),
		true,
		true)
	pages.AddPage("Menu", list, true, false)
	pages.AddPage("Add User", form, true, false)
	pages.AddPage("Add Domain", dmnform, true, false)
	pages.AddPage("Update User Password", upwform, true, false)
	pages.AddPage("Delete User Account", rmusrform, true, false)
	pages.AddPage("Whitelist", wlform, true, false)
	pages.AddPage("Blacklist", blform, true, false)

	// main menu
	list.AddItem("Add user account", "Add a user account to the server", 'a', func() {
		form.Clear(true)
		addUserForm()
		pages.SwitchToPage("Add User")
	})
	list.AddItem("Add domain name", "Add a domain name to this server", 'b', func() {
		dmnform.Clear(true)
		addDomainForm()
		pages.SwitchToPage("Add Domain")
	})
	list.AddItem("Update user password", "Update a users password", 'c', func() {
		upwform.Clear(true)
		updateUserPassForm()
		pages.SwitchToPage("Update User Password")
	})
	list.AddItem("Delete user account", "Will not remove maildir", 'd', func() {
		rmusrform.Clear(true)
		deleteUser()
		pages.SwitchToPage("Delete User Account")
	})
	list.AddItem("Whitelist (SpamAssassin)", "Whitelist a domain/email", 'e', func() {
		wlform.Clear(true)
		whitelist()
		pages.SwitchToPage("Whitelist")
	})
	list.AddItem("Blacklist (SpamAssassin)", "Blacklist a domain/email", 'f', func() {
		blform.Clear(true)
		blacklist()
		pages.SwitchToPage("Blacklist")
	})

	var inputs = []tview.Primitive{pages, list}

	app.SetInputCapture(func(event *tcell.EventKey) *tcell.EventKey {
		if event.Key() == tcell.KeyCtrlC {
			app.Stop()
		} else if event.Key() == tcell.KeyEscape {
			pages.SwitchToPage("Welcome")
		} else if event.Key() == tcell.KeyTab {
			cycleFocus(app, inputs)
		}
		return event
	})

	grid := tview.NewGrid().
		SetRows(2, 0, 2).
		SetColumns(40, 60).
		SetBorders(true).
		AddItem(newPrimitive("Tapioca Mail Server Manager"), 0, 0, 1, 3, 0, 0, false).
		AddItem(newPrimitive("^C to quit    <esc> to return to menu    <tab> to switch panes"), 2, 0, 1, 3, 0, 0, false).
		SetBordersColor(tcell.Color57)

	// grid.AddItem(list, 0, 0, 0, 0, 0, 0, false).
	// 	AddItem(pages, 1, 0, 1, 3, 0, 0, false)

	grid.AddItem(list, 1, 0, 1, 1, 0, 100, false).
		AddItem(pages, 1, 1, 1, 2, 1, 100, false)

	if err := app.SetRoot(grid, true).Run(); err != nil {
		panic(err)
	}
}

func addDomainForm() {
	domain := Domain{}

	dmnform.AddInputField("Domain", "", 20, nil, func(name string) {
		domain.name = name
	})

	dmnform.AddButton("Save", func() {
		domains = append(domains, domain)
		pages.SwitchToPage("Menu")
	})
}

func addUserForm() {
	user := User{}

	form.AddInputField("Email", "", 20, nil, func(email string) {
		user.email = email
	})

	form.AddPasswordField("Password", "", 20, '*', func(password string) {
		user.password = password
	})

	form.AddButton("Save", func() {
		users = append(users, user)
		pages.SwitchToPage("Menu")
	})
}

func updateUserPassForm() {
	userpw := UserPW{}

	upwform.AddInputField("Email", "", 20, nil, func(email string) {
		userpw.email = email
	})

	upwform.AddPasswordField("New password", "", 20, '*', func(newpw string) {
		userpw.newpw = newpw
	})

	upwform.AddButton("Save", func() {
		userpws = append(userpws, userpw)
		pages.SwitchToPage("Menu")
	})
}

func deleteUser() {
	deluser := DelUser{}

	rmusrform.AddInputField("Email", "", 20, nil, func(email string) {
		deluser.email = email
	})

	rmusrform.AddButton("Save", func() {
		delusers = append(delusers, deluser)
		pages.SwitchToPage("Menu")
	})
}

func whitelist() {
	whitelist := Whitelist{}

	wlform.AddInputField("Email/domain to whitelist", "", 20, nil, func(email string) {
		whitelist.email = email
	})

	wlform.AddButton("Save", func() {
		whitelists = append(whitelists, whitelist)
		pages.SwitchToPage("Menu")
	})
}

func blacklist() {
	blacklist := Blacklist{}

	blform.AddInputField("Email/domain to blacklist", "", 20, nil, func(email string) {
		blacklist.email = email
	})

	blform.AddButton("Save", func() {
		blacklists = append(blacklists, blacklist)
		pages.SwitchToPage("Menu")
	})
}
